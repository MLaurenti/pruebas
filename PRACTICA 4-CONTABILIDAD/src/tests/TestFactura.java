package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestFactura {
		
		static GestorContabilidad gestorPruebas;
		static Factura unaFactura;
	@BeforeClass
	static public void crearGestor() {
		gestorPruebas= new GestorContabilidad();
		
	}
	
	@Before
	public void setUp(){
		
		gestorPruebas.listaFacturas.clear();
		unaFactura=new Factura();
	}
	
	/**
	 * Test que calcula el precio total de cada factura
	 */
	@Test
	public void testCalcularPrecioTotal() {
		
		
		unaFactura.setPrecioUnidad(6F);
		unaFactura.setCantidad(5);
		
		float actual =unaFactura.calcularPrecioTotal();
		float esperado=30F;
		
		assertEquals(actual, esperado, 0.0);
		
	}
	
	
	/**
	 * Test que calcula el precio total de cada factura cuando el precio es cero
	 */
	@Test
	public void testCalcularPrecioValorCero() {
		
		
		unaFactura.setPrecioUnidad(0F);
		unaFactura.setCantidad(5);
		
		float actual =unaFactura.calcularPrecioTotal();
		float esperado=0F;
		
		assertEquals(actual, esperado, 0.0);
	}
	
	/**
	 * Test que calcula el precio total de cada factura cuando la cantidad es cero
	 */
	@Test
	public void testCalcularPrecioTotalCantidadCero() {
		
		
		unaFactura.setPrecioUnidad(6F);
		unaFactura.setCantidad(0);
		
		float actual =unaFactura.calcularPrecioTotal();
		float esperado=0F;
		
		assertEquals(actual, esperado, 0.0);
	}
	
	/**
	 * Test que comprueba que una factura no existe al pasarle el codigo de la misma.
	 */
	@Test
	public void testBuscarFacturaInexistente() {
		
		String codigoFactura="78951";
		Factura actual=gestorPruebas.buscarFactura(codigoFactura);
		assertNull(actual);
	}
	
	/**
	 * Test que comprueba que una factura existe al pasarle el codigo
	 */
		@Test
		public void testBuscarFacturaExistente() {
		
		Cliente nuevoCliente=new Cliente("Sergio", "458632", LocalDate.of(2018, 1, 8));	
		
		unaFactura.setCodigoFactura("13456");
		unaFactura.setCliente(nuevoCliente);
		gestorPruebas.listaFacturas.add(unaFactura);
		
		Factura actual= gestorPruebas.buscarFactura("13456");
		assertEquals(unaFactura, actual);
		
	}
		
	/**
	 * Test que comprueba que una factura con un codigo ya existente no se cree.
	 */
		@Test
		public void crearFacturaConCodigoExistente() {
			
			Cliente nuevo= new Cliente("Pedro", "234567P", LocalDate.of(2017, 11, 6));
			
			unaFactura.setCodigoFactura("8965");
			unaFactura.setCliente(nuevo);
			gestorPruebas.listaFacturas.add(unaFactura);
			
			Factura actual= gestorPruebas.buscarFactura("8965");
			assertEquals(unaFactura, actual);
		}
		/**
		 * Test que comprueba que una factura con un codigo que no existe se cree.
		 */
		@Test
		public void crearFacturaConCodigoNuevo() {
			
			String codigoFactura="8971";
			
			Factura actual= gestorPruebas.buscarFactura(codigoFactura);
			assertNull(actual);
		}
		/**
		 * Test que comprueba que entre todas las facturas, se devuelva la m�s cara
		 */
		@Test
		public void facturaMasCara() {
			
			
			unaFactura.setCantidad(2);
			unaFactura.setPrecioUnidad(125);
			gestorPruebas.listaFacturas.add(unaFactura);
			
			Factura otraFactura= new Factura();
			otraFactura.setCantidad(2);
			otraFactura.setPrecioUnidad(85);
			gestorPruebas.listaFacturas.add(otraFactura);
			
			Factura esperado= unaFactura;
			Factura actual=gestorPruebas.facturaMasCara();
			assertEquals(esperado,actual);
		}
		/**
		 * Test que comprueba que entre todas las facturas habiendo una con precio
		 * negativo, se devuelva la m�s cara
		 */
		@Test
		public void facturaNegativaMasCara() {
			
			
			unaFactura.setCantidad(2);
			unaFactura.setPrecioUnidad(125);
			gestorPruebas.listaFacturas.add(unaFactura);
			
			Factura otraFactura= new Factura();
			otraFactura.setCantidad(2);
			otraFactura.setPrecioUnidad(-85);
			gestorPruebas.listaFacturas.add(otraFactura);
			
			Factura esperado= unaFactura;
			Factura actual=gestorPruebas.facturaMasCara();
			assertEquals(esperado,actual);
		}
		@Test
		public void facturaMasCaraSinFacturas() {
			
			Factura actual= gestorPruebas.facturaMasCara();
			assertNull(actual);
		}
		
		/**
		 * Test que comprueba que el metodo facturacionAnual devuelva la facturacion anual
		 * si hay facturas existente con ese a�o de creacion de facturas
		 */
		@Test
		public void annoCorrectoFacturacion() {
			
			
			unaFactura.setFecha(LocalDate.of(2014, 11, 6));
			unaFactura.setCantidad(1);
			unaFactura.setPrecioUnidad(50);
			gestorPruebas.listaFacturas.add(unaFactura);
			
			Factura otraFactura= new Factura();
			otraFactura.setFecha(LocalDate.of(2014, 9, 29));
			otraFactura.setCantidad(1);
			otraFactura.setPrecioUnidad(95);
			gestorPruebas.listaFacturas.add(otraFactura);
			
			float esperado =  145;
			float actual= gestorPruebas.calcularFacturacionAnual(2014);
			assertEquals(esperado,actual, 0.0);

		}
		/**
		 * Test que comprueba que el metodo facturacionAnual no devuelva la facturacion anual
		 * si no hay ninguna factura existente con ese a�o de creacion de factura
		 */
		@Test
		public void annoIncorrectoFacturacion() {
			
			
			unaFactura.setFecha(LocalDate.of(2014, 11, 6));
			unaFactura.setCantidad(1);
			unaFactura.setPrecioUnidad(50);
			gestorPruebas.listaFacturas.add(unaFactura);
			
			Factura otraFactura= new Factura();
			otraFactura.setFecha(LocalDate.of(2014, 9, 29));
			otraFactura.setCantidad(1);
			otraFactura.setPrecioUnidad(95);
			gestorPruebas.listaFacturas.add(otraFactura);
			
			float esperado =  0;
			float actual= gestorPruebas.calcularFacturacionAnual(2017);
			assertEquals(esperado,actual, 0.0);

		}
		/**
		 * Metodo que comprueba que se asigna correctamente un cliente con un DNI a una factura
		 * con un codigo determinado
		 */
		@Test
		public void asignarClienteAsistenteAFactura() {
			gestorPruebas.listaClientes.clear();
		
			
			Cliente unCliente= new Cliente("894563S");
			gestorPruebas.listaClientes.add(unCliente);
			
			
			unaFactura.setCodigoFactura("45");
			unaFactura.setCliente(unCliente);
			
			gestorPruebas.asignarClienteAFactura("894563S", "45");
			Cliente esperado= unCliente;
			Cliente actual= unaFactura.getCliente();
			
			assertEquals(esperado, actual);
		}
		/**
		 * Metodo que comprueba si la cantidad de facturas por cliente es correcta, cuando �ste
		 * existe al buscarlo por el DNI
		 */
		@Test
		public void cantidadFacturasClienteExistente() {
			gestorPruebas.listaClientes.clear();
		
			Cliente unCliente= new Cliente("894563S");
			gestorPruebas.listaClientes.add(unCliente);
			
			Factura unaFactura= new Factura();
			unaFactura.setCantidad(1);
			unaFactura.setCliente(unCliente);
			gestorPruebas.listaFacturas.add(unaFactura);
			
			int esperado=1;
			int actual= gestorPruebas.cantidadFacturasPorClientes("894563S");
			assertEquals(esperado,actual);
			
		}
		/**
		 * Metodo que comprueba si la cantidad de facturas por cliente es correcta, cuando �ste
		 *  no existe al buscarlo por el DNI
		 */
		@Test
		public void cantidadFacturasClienteInexistente() {
			gestorPruebas.listaClientes.clear();
		
			Cliente unCliente= new Cliente("894563S");
			
			unaFactura.setCantidad(2);
			unaFactura.setCliente(unCliente);
			
			int esperado=0;
			int actual= gestorPruebas.cantidadFacturasPorClientes("741562R");
			assertEquals(esperado,actual);
			
		}
		/**
		 * Metodo que comprueba si se elimina una factura, cuando se busca a �sta en la lista
		 * a trav�s de su codigo y no existe
		 */
		@Test
		public void eliminarFacturaInexistente() {
			
			
			
			unaFactura.setCodigoFactura("8");
			
			gestorPruebas.crearFactura(unaFactura);
			gestorPruebas.eliminarFactura("9");
			
			boolean actual=gestorPruebas.listaFacturas.contains(unaFactura);
			
			assertFalse(actual);
		}
		/**
		 * Metodo que comprueba si se elimina una factura, cuando se busca a �sta en la lista
		 * a trav�s de su codigo y si que existe
		 */
		@Test
		public void eliminarFacturaExistente() {
			
			
			
			unaFactura.setCodigoFactura("8");
			
			gestorPruebas.listaFacturas.add(unaFactura);
			gestorPruebas.eliminarFactura("8");
			
			boolean actual=gestorPruebas.listaFacturas.contains(unaFactura);
			
			assertTrue(actual);
		}
		
		
		
}