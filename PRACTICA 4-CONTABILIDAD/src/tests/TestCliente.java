package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestCliente {

		static GestorContabilidad gestorPruebas;
		static Cliente unCliente;

	@BeforeClass
	static public void crearGestor() {
		gestorPruebas=new GestorContabilidad();
	
	}
	@Before
	public void setUp(){
		gestorPruebas.listaClientes.clear();
		unCliente=new Cliente();
	}
	/**
	 * Test que busca en la lista a un cliente que no existe en la misma
	 */
	@Test
	public void testBuscarClienteInexistente() {
		
		String dni="85369871";
		Cliente actual=gestorPruebas.buscarCliente(dni);
		assertNull(actual);
	}
	/**
	 * Test que busca a un cliente que existe en la lista
	 */
	@Test
	public void testBuscarClienteExistente() {
		
		unCliente.setDni("458632");
		
		gestorPruebas.listaClientes.add(unCliente);
		
		String dni="85369871";
		Cliente actual=gestorPruebas.buscarCliente(dni);
		assertNull(actual);
		
	}
	
	
	/**
	 * Test que comprueba que no podemos dar de alta a un cliente que ya existe con el mismo dni
	 */
	@Test
	public void altaClienteExistente() {
		
		
		unCliente.setDni("458632");
		gestorPruebas.listaClientes.add(unCliente);
		
		Cliente actual= gestorPruebas.buscarCliente("458632");
		assertEquals(unCliente, actual);
	}
	/**
	 * Test que comprueba que podemos dar de alta a un cliente nuevo
	 */
	@Test
	public void altaClienteNuevo() {
		
		String dni="96321";
	
		Cliente actual= gestorPruebas.buscarCliente(dni);
		assertNull(actual);
	}
	/**
	 * Test que comprueba si dentro de la lista de Clientes el metodo clienteMasAntiguo nos devuelve
	 * el cliente con la fecha de alta mas antigua
	 */
	@Test
	public void clienteMasAntiguo() {
		
		
		unCliente.setFechaAlta(LocalDate.of(2018, 2, 6));
		gestorPruebas.listaClientes.add(unCliente);
		
		Cliente otroCliente= new Cliente();
		otroCliente.setFechaAlta(LocalDate.of(2014, 11, 8));
		gestorPruebas.listaClientes.add(otroCliente);
		Cliente esperado=  otroCliente;
		Cliente actual=gestorPruebas.clienteMasAntiguo();
		assertEquals(esperado, actual );
		
		
	}
	/**
	 * Test que comprueba que no haya clientes en la lista para que no haya errores en el metodo 
	 * clienteMasAntiguo
	 */
	@Test
	public void clienteMasAntiguoSinClientes() {
		
		Cliente actual= gestorPruebas.clienteMasAntiguo();
		assertNull(actual);
		
	}
	/**
	 * Test que comprueba que un cliente existe para poder eliminarlo de la lista
	 */
	@Test
	public void eliminarClienteExistente() {
		
		
		unCliente.setDni("2345678K");
		
		gestorPruebas.altaCliente(unCliente);
		gestorPruebas.eliminarClientes("2345678K");
		
		
		boolean actual=gestorPruebas.listaClientes.contains(unCliente);
		
		assertFalse(actual);
	}
	/**
	 * Test que comprueba si el Cliente no existe para eliminarlo.
	 */
	@Test
	public void eliminarClienteInexistente() {
		
		
		unCliente.setDni("2345678K");
		
		gestorPruebas.altaCliente(unCliente);
		gestorPruebas.eliminarClientes("7856678K");
		
		
		boolean actual=gestorPruebas.listaClientes.contains(unCliente);
		
		assertTrue(actual);
	}
	/**
	 *Test que comprueba que un cliente no tenga facturas para eliminarlo
	 */
	
	@Test
	public void eliminarClienteSinFacturas() {
		
		
		unCliente.setDni("2345678K");
		
		gestorPruebas.altaCliente(unCliente);
		gestorPruebas.eliminarClientes("2345678K");
		
		
		boolean actual=gestorPruebas.listaClientes.contains(unCliente);
		
		assertFalse(actual);
	}
}
