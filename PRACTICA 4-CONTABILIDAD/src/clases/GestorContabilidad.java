package clases;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

public class GestorContabilidad {
/*La clase GestorContabilidad contiene 2 arrayLists: listaFacturas que almacena objetos de tipo
Factura y listaClientes que almacena los Clientes. */
	
	public ArrayList<Cliente> listaClientes;
	public ArrayList<Factura> listaFacturas;
	
	public GestorContabilidad() {
		listaClientes=new ArrayList<>();
		listaFacturas=new ArrayList<>();
	}
	/*Cliente buscarCliente(String dni): devuelve un objeto de tipo Cliente con el cliente de
listaClientes que tiene ese dni. Si no existe devuelve null.*/
	public Cliente buscarCliente(String dni) {
		for (int i = 0; i < listaClientes.size(); i++) {
			if(listaClientes.get(i).getDni().equals(dni)) {
				return listaClientes.get(i);
			}
		}
		return null;
	}
	/*Factura buscarFactura(String c�digo): devuelve el objeto Factura de la listaFacturas que
coincida con dicho c�digo. Si no existe devuelve null.*/
	
	public Factura buscarFactura(String codigo) {
		for (int i = 0; i < listaFacturas.size(); i++) {
			if(listaFacturas.get(i).getCodigoFactura().equals(codigo)) {
				return listaFacturas.get(i);
			}
		}
		return null;
	}
	public boolean existeCliente(String dni) {
		for (int i = 0; i < listaClientes.size(); i++) {
			if(dni.equals(listaClientes.get(i).getDni())) {
				return true;
			}
		}
		return false;
	}
	/*void altaCliente(Cliente cliente): recibe un objeto de tipo Cliente y lo a�ade a la lista de
clientes. Este m�todo debe comprobar que el cliente que a�adimos no tiene un dni que ya
tenemos en la lista. Si el dni ya existe no a�adir� al cliente*/
	public void altaCliente(Cliente cliente) {
	
			
			if(existeCliente(cliente.getDni())==false) {
				listaClientes.add(cliente);
			}
		}
	

	public void listarClientes() {
		for (int i = 0; i < listaClientes.size(); i++) {
			System.out.println(listaClientes.get(i));
		}
	}
	/*void crearFactura(Factura factura): recibe un objeto Factura y lo a�ade a la lista de clientes.
Debe comprobar que el c�digo de la factura no est� repetido.
*/

	public boolean existeFactura(String codigo) {
		for (int i = 0; i < listaFacturas.size(); i++) {
			if(codigo.equals(listaFacturas.get(i).getCodigoFactura())) {
				return true;
			}
		}
		return false;
	}
	public void crearFactura(Factura factura) {
		
			
			if(existeFactura(factura.getCodigoFactura())==false) {
				listaFacturas.add(factura);
			}
		}
	
	/*Cliente clienteMasAntiguo(): devuelve el objeto Cliente con la fecha de alta m�s antigua. Si no
existe ning�n cliente, devuelve null.*/
	public Cliente clienteMasAntiguo() {
		if( !listaClientes.isEmpty()) {
		LocalDate fechaMax=listaClientes.get(0).getFechaAlta();
		int aux=0;
		for (int i = 0; i < listaClientes.size(); i++) {
			if(listaClientes.get(i).getFechaAlta().isBefore(fechaMax)) {
				fechaMax=listaClientes.get(i).getFechaAlta();
				aux=i;
				return (Cliente) listaClientes.get(aux);
			}
		}}
		return null;
	}

	public void listarFacturas() {
		for (int i = 0; i < listaFacturas.size(); i++) {
			System.out.println(listaFacturas.get(i));
		}
	}
	
	/*Factura facturaMasCara(): devuelve el objeto Factura con la factura m�s cara. Si no hay
facturas devuelve null.*/
	public Factura facturaMasCara() {
		float precioMax=0;
		int aux=0;
		if(!listaFacturas.isEmpty()) {
		for (int i = 0; i < listaFacturas.size(); i++) {
			if(((listaFacturas.get(i).getPrecioUnidad())*(listaFacturas.get(i).getCantidad()))>precioMax) {
				precioMax=(listaFacturas.get(i).getPrecioUnidad()*listaFacturas.get(i).getCantidad());
				aux=i;
				return listaFacturas.get(aux);
			}
		}}
		return null;
	}
	/*float calcularFacturacionAnual(int anno): devuelve el dinero facturado en el a�o introducido
por par�metro.*/
	
	public float calcularFacturacionAnual(int anno) {
		float aux=0;
		for (int i = 0; i < listaFacturas.size(); i++) {
		if(anno==listaFacturas.get(i).getFecha().getYear()) {
			aux=aux+ listaFacturas.get(i).calcularPrecioTotal();
				
		}		
		
		}
		
		return aux;
	}
	/*void asignarClienteAFactura(String dni, String codigoFactura): asigna al campo cliente del
objeto factura con dicho c�digo, el objeto cliente con dicho dni.*/
	public void asignarClienteAFactura(String dni, String codigoFactura) {
		for (int i = 0; i < listaClientes.size(); i++) {
			if(existeCliente(dni) && existeFactura(codigoFactura)) {
				Cliente uno= new Cliente(dni);
				listaFacturas.get(i).setCliente(uno);
			}
		}
	}
	
	/*int cantidadFacturasPorCliente(String dni): devuelve un entero con la cantidad de facturas del
cliente con dicho dni.*/
	public int cantidadFacturasPorClientes(String dni) {
		int contador=0;
		for (int i = 0; i < listaFacturas.size(); i++) {
		if(dni.equals(listaFacturas.get(i).getCliente().getDni())) {
			
			contador++;
		}
	
		}
		return contador;
	}
	
	/*void eliminarFactura(String c�digo): elimina la factura con dicho c�digo.*/
	public void eliminarFactura(String codigo) {
		for (int i = 0; i < listaFacturas.size(); i++) {
			if(buscarFactura(codigo)==null) {
				listaFacturas.remove(i);
			}
		}
		
	}
	/*void eliminarCliente(String dni): elimina al cliente con dicho dni. Solo se permite eliminar a los
clientes que no tienen facturas.*/
	public void eliminarClientes(String dni) {
		for (int i = 0; i < listaClientes.size(); i++) {
			if(buscarCliente(dni)!=null && cantidadFacturasPorClientes(dni)==0) {
				listaClientes.remove(i);
			}
		}
		
	}
}
