package clases;

import java.time.LocalDate;

public class Cliente {
/*La clase Cliente tiene como atributos: nombre, dni, fechaAlta.*/
	private String nombre;
	private String dni;
	private LocalDate fechaAlta;
	
	public Cliente(String dni) {
		this.dni=dni;
	}
	public Cliente() {
		
	}
	public Cliente(String nombre, String dni, LocalDate fechaAlta) {
		super();
		this.nombre = nombre;
		this.dni = dni;
		this.fechaAlta = fechaAlta;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public LocalDate getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	@Override
	public String toString() {
		return nombre + " " + dni + " " + fechaAlta;
	}
	
	
}
