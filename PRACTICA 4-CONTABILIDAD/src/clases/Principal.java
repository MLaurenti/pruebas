package clases;

import java.time.LocalDate;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		GestorContabilidad nuevoGestor= new GestorContabilidad();
		Cliente unCliente= new Cliente("Sergio", "8645", LocalDate.of(2017, 11, 12));
		//nuevoGestor.altaCliente(unCliente);
		
		Cliente otroCliente= new Cliente("Pedro", "9876", LocalDate.of(2015, 11, 6));
		//nuevoGestor.altaCliente(otroCliente);
		
		Factura uno= new Factura("1", LocalDate.of(2017, 11, 12), "Contaplus", 125, 1, unCliente);
		Factura dos= new Factura("2", LocalDate.of(2015, 11, 6), "Nominaplus", 105, 2, otroCliente);
		Factura tres= new Factura("3", LocalDate.of(2015, 11, 6), "Nominaplus", 105, 2, otroCliente);
		nuevoGestor.crearFactura(uno);
		nuevoGestor.crearFactura(dos);
		nuevoGestor.crearFactura(tres);
		
		System.out.println(nuevoGestor.clienteMasAntiguo());
		System.out.println(nuevoGestor.facturaMasCara());
		System.out.println(nuevoGestor.calcularFacturacionAnual(2017));
		System.out.println(nuevoGestor.cantidadFacturasPorClientes("9876"));
		
	}

}
